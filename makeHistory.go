package price

import (
	"cryptoalerts/base"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"time"

	"gopkg.in/mgo.v2"

	base360 "coin360/base"
	cap360 "coin360/coinmarketcap"

	"github.com/namsral/flag"
)

//Row Row
type Row struct {
	T      int64
	Open   float64
	Close  float64
	High   float64
	Low    float64
	Cap    int64
	Volume int64
}

//HistoryMaker HistoryMaker
type HistoryMaker struct {
	sessionPool *base.MgoSessionPool
	db          base.DB
	logger      *base.Logger
	r           *rand.Rand
}

//Stop Stop
func (s HistoryMaker) Stop() {
	s.sessionPool.Close()

}

func spec() map[string]*base360.InstrumentSpecification {
	l := base360.NewLogger("", true, true, true)
	fetcher := cap360.NewInstrumentLoader(l)
	specs, _ := fetcher.FetchSpecification()
	index := make(map[string]*base360.InstrumentSpecification)
	for _, s := range specs {
		index[s.Slug] = s
	}
	return index
}
func f(specs map[string]*base360.InstrumentSpecification, rows map[string][]Row) map[string][]Row {
	out := make(map[string][]Row)
	for _, s := range specs {
		if r, ok := rows[s.Slug]; ok {
			out[s.Symbol] = r
		}
	}
	return out
}

func readSymbolFile(path string) map[string][]base.Currency {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		fmt.Println("Read file error", err, path)
		return nil
	}
	var curs map[string][]base.Currency
	err = json.Unmarshal(data, &curs)
	if err != nil {
		fmt.Println("Json error:", err)
	}
	return curs
}

func (s *HistoryMaker) load() map[string][]Row {
	out := make(map[string][]Row)
	s.sessionPool.Run(func(sess *mgo.Session) error {
		d := sess.DB("cmchistory")
		names, _ := d.CollectionNames()
		for _, n := range names {
			var r []Row
			d.C(n).Find(nil).Sort("t").All(&r)
			out[n] = r
		}
		return nil
	})
	return out
}

type PricesBySource struct {
	Cur    base.Currency
	Prices []Row
}

func (s *HistoryMaker) sendRow(market string, cur base.Currency, row Row) {
	price := base.Price{}

	price.Source.Market = market
	price.Source.Currency = cur
	price.Quote.Price = row.Open
	price.Quote.TimeInMs = (row.T) * 1000
	savePrice(s.sessionPool, &price)

	price.Quote.Price = row.Close
	price.Quote.TimeInMs = (row.T + 24*60*60 - 10*60) * 1000
	savePrice(s.sessionPool, &price)

	r := s.r.Int31n(11)
	var addHourHigh int64 = 12
	var addHourLow int64 = 6
	if r < 6 {
		addHourHigh = 6
		addHourLow = 12
	}
	price.Quote.Price = row.High
	price.Quote.TimeInMs = (row.T + addHourHigh*60*60) * 1000
	savePrice(s.sessionPool, &price)
	price.Quote.Price = row.Low
	price.Quote.TimeInMs = (row.T + addHourLow*60*60) * 1000
	savePrice(s.sessionPool, &price)
}
func (s *HistoryMaker) all(curs map[string][]base.Currency) {
	logger := s.logger
	rows := s.load()
	specs := spec()
	cursPrices := f(specs, rows)
	for market, curByMarket := range curs {
		logger.Info("Calculate for:", market)
		for _, cur := range curByMarket {
			baseCurPrices, ok := cursPrices[cur.Base]
			if !ok {
				s.logger.Error(cur.Base, "not found")
				continue
			}
			if cur.Quoted == "USD" {
				for _, row := range baseCurPrices {
					s.sendRow(market, cur, row)
				}
				continue
			}
			logger.Info("Start calculate cross:", cur.ToString())
			quoted, ok := cursPrices[cur.Quoted]
			if !ok {
				s.logger.Error(cur.Quoted, "not found")
				continue
			}
			for _, q := range quoted {
				for _, b := range baseCurPrices {
					if q.T == b.T {
						var row Row
						row.T = q.T
						row.Open = b.Open / q.Open
						row.Close = b.Close / q.Close
						row.High = b.High / q.High
						row.Low = b.Low / q.Low
						row.Volume = int64(float64(b.Volume) / q.Close)
						row.Cap = int64(float64(b.Cap) / q.Close)
						s.sendRow(market, cur, row)
						// cross = append(cross, row)
					}
				}
			}
		}

	}
}

//Tool Tool
func (s HistoryMaker) Tool(args []string, flags *flag.FlagSet, isStopped chan int) (progressFunc func() error, err error) {

	base.LoggerParamsAdd(flags)
	base.AddDBFlags(flags)
	filename := flags.String("file", "", "")

	flags.Usage = flags.PrintDefaults
	if err = flags.Parse(args); err != nil {
		return
	}
	logger := base.NewLoggerFromFlags(flags)
	s.logger = logger

	s.r = rand.New(rand.NewSource(time.Now().UnixNano()))
	s.db = base.MakeDBFromFlags(flags)
	logger.Info("Try create DB saver", s.db)
	s.sessionPool, err = base.NewMgoSessionPool(s.db)
	if err != nil {
		return
	}
	curs := readSymbolFile(*filename)
	logger.Info(curs)
	s.all(curs)
	logger.Info("Done:", err)
	isStopped <- 0
	progressFunc = func() error {
		return nil
	}

	logger.Info("AlertSaver tool init ok")
	return
}
