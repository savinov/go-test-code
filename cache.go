package price

import (
	"cryptoalerts/base"
	"cryptoalerts/message"
	"cryptoalerts/rmq"
	"encoding/json"
	"sync"
	"time"

	"github.com/namsral/flag"
	"github.com/pkg/errors"
)

const (
	MakeCacheRoutingKey = "cache-maker"
)

type cacheMakerRequest struct {
	Exchange string `json:"exchange"`
}

type cache struct {
	BySymbol        map[string]json.RawMessage `json:"by_symbol"`
	Exchange        string                     `json:"exchange"`
	UpdateTimeInSec int64                      `json:"update_time_in_sec"`
}

//Cacher Cacher
type Cacher struct {
	apiConsumer    *rmq.Consumer
	apiPublisher   *rmq.Publisher
	dbConsumer     *rmq.Consumer
	dbPublisher    *rmq.Publisher
	logger         *base.Logger
	cache          *cache
	exchange       string
	mutex          sync.Mutex
	lastUpdateTime *int64
}

func (s *Cacher) OnMessageImpl(mess message.Message) (err error) {
	logger := s.logger
	logger.Trace("mess.Kind", mess.Kind.ToString())
	switch mess.Kind {
	case message.KindPriceCacheUpdated:
		var cache cache
		err = json.Unmarshal(mess.Obj, &cache)
		if err == nil && cache.Exchange != s.exchange {
			logger.Warning("Incorrect exchange. Expected:", s.exchange, "received:", cache.Exchange)
			return nil
		}
		if err == nil {
			s.mutex.Lock()
			s.cache = &cache
			s.mutex.Unlock()
			*s.lastUpdateTime = time.Now().Unix()
			// for s, v := range s.cache.BySymbol {
			// 	logger.Trace(s, string(v))
			// }
			logger.Info("cache updated for:", s.exchange, "time:", *s.lastUpdateTime,
				"size")
		}
	case message.KindPriceGetBySource:
		var srcByPeriod base.SourceByPeriod
		err = json.Unmarshal(mess.Obj, &srcByPeriod)
		if err != nil {
			logger.Trace("Unmarshal json error:", err)
			break
		}
		src := srcByPeriod.Source
		var messReply message.Message
		if err == nil && src.Market != s.exchange {
			//Don't answer on incorrect exchange
			logger.Trace("incorrect exchange:", src.Market, "epected:", s.exchange)
			return
		}
		if src.GetStrID() == "" {
			err = errors.New("incorrect incoming name")
		}
		if err == nil && s.cache == nil {
			err = errors.New("cache is null")
		}
		if err == nil && s.cache.BySymbol == nil {
			err = errors.New("symbol map is empty")
		}
		name := src.Currency.ToString()
		logger.Debug("get cache for:", name)
		if err == nil {
			s.mutex.Lock()
			if val, ok := s.cache.BySymbol[name]; val != nil && ok {
				messReply, err = message.NewByJSON(message.KindPriceGetBySource, val, mess.ID, mess.ReplyTo)
			} else {
				err = errors.New("Symbol not found")
			}
			s.mutex.Unlock()
		}
		if err != nil {
			messReply, err = message.Make(message.KindPriceError, err.Error(), mess.ID, mess.ReplyTo)
		}
		if err != nil {
			logger.Debug("Error getPrice:", err)
		}
		s.apiPublisher.Publish(mess.ReplyTo, messReply)
	default:
		s.logger.Warning("Unknown message:", mess.Kind, mess.Obj)
	}
	return
}

//OnMessage OnMessage
func (s *Cacher) OnMessage(mess message.Message) (err error) {
	return s.OnMessageImpl(mess)
}

//Stop Stop
func (s *Cacher) Stop() {
	s.apiConsumer.StopAndClose()
	s.dbConsumer.StopAndClose()

	s.apiPublisher.Close()
	s.dbPublisher.Close()

}

//Tool Tool
func (s *Cacher) Tool(args []string, flags *flag.FlagSet, isStopped chan int) (progressFunc func() error, err error) {
	base.LoggerParamsAdd(flags)
	rmq.AddFlags(flags, "con_api", rmq.IsConsumer, "receive requests from api")
	rmq.AddFlags(flags, "pub_api", rmq.IsPubliher, "send reply for api")

	rmq.AddFlags(flags, "con_db", rmq.IsConsumer, "receive requests")
	rmq.AddFlags(flags, "pub_db", rmq.IsPubliher, "send reply and make cache request")
	exchange := flags.String("exchange", "", "Excnahge name")

	timeout := flags.Int64("timeout", 30, "update cache timeout")
	flags.Usage = flags.PrintDefaults
	if err = flags.Parse(args); err != nil {
		return
	}
	logger := base.NewLoggerFromFlags(flags)
	s.logger = logger
	s.exchange = *exchange
	s.dbConsumer, err = rmq.NewConsumerFromFlags(logger, flags, "con_db")
	if err != nil {
		return
	}

	s.dbPublisher, err = rmq.NewPublisherFromFlags(logger, flags, "pub_db")
	if err != nil {
		return
	}
	s.apiConsumer, err = rmq.NewConsumerFromFlags(logger, flags, "con_api")
	if err != nil {
		return
	}

	s.apiPublisher, err = rmq.NewPublisherFromFlags(logger, flags, "pub_api")
	if err != nil {
		return
	}
	go func() {
		logger.Info("Start consume")
		err := s.dbConsumer.Consume(s)
		if err != nil {
			isStopped <- 1
			return
		}
		isStopped <- 1
	}()
	go func() {
		logger.Info("Start consume")
		err := s.apiConsumer.Consume(s)
		if err != nil {
			isStopped <- 1
			return
		}
		isStopped <- 1
	}()
	reloadCache := func(onStart bool) error {
		req := cacheMakerRequest{Exchange: *exchange}
		kind := message.KindPriceMakeCache
		if onStart {
			kind = message.KindPriceGetHotCache
		}
		mess, err := message.NewWithReply(kind, &req, s.dbConsumer.GetRoutingKey())
		if err != nil {
			err = errors.Wrap(err, "make message")
			return err
		}
		err = s.dbPublisher.Publish("", mess)
		if err != nil {
			err = errors.Wrap(err, "publish request on make cache error")
		}
		return err
	}

	t := time.Now().Unix()
	s.lastUpdateTime = &t
	if err = reloadCache(true); err != nil {
		return
	}
	progressFunc = func() error {
		now := time.Now().Unix()
		if *s.lastUpdateTime+*timeout <= now {
			logger.Info("Reload cache. Last:", *s.lastUpdateTime, "now:", now)
			return reloadCache(false)
		}
		return nil
	}

	logger.Info("Cacher tool init ok")
	return
}
