package price

import (
	"cryptoalerts/base"

	"github.com/namsral/flag"
)

//ChartCalculator ChartCalculator
type ChartCalculator struct {
	dbForLoad base.DB
	dbForSave base.DB
	db        *base.DBSaver
	logger    *base.Logger
}

//Stop Stop
func (s ChartCalculator) Stop() {
	s.db.Close()
}

//Tool Tool
func (s ChartCalculator) Tool(args []string, flags *flag.FlagSet, isStopped chan int) (progressFunc func() error, err error) {

	base.LoggerParamsAdd(flags)
	base.AddDBFlags(flags)
	flags.Usage = flags.PrintDefaults
	if err = flags.Parse(args); err != nil {
		return
	}
	logger := base.NewLoggerFromFlags(flags)
	s.logger = logger
	if err != nil {
		return
	}

	db := base.MakeDBFromFlags(flags)
	logger.Info("Try create DB saver", db)
	s.db, err = base.NewDBSaver(logger, db)
	if err != nil {
		return
	}
	logger.Info("DB saver created")
	progressFunc = func() error {

		return nil
	}

	logger.Info("AlertSaver tool init ok")
	return
}
