package price

import (
	"cryptoalerts/base"
	"cryptoalerts/message"
	"cryptoalerts/rmq"
	"cryptoalerts/user"
	"encoding/json"
	"errors"
	"log"

	"github.com/namsral/flag"
	"github.com/valyala/fasthttp"
)

//APIGetCandleParams APIGetCandleParams
type APIGetCandleParams struct {
	User   base.UserInfo    `json:"user"`
	Source base.PriceSource `json:"source"`
}

//APIGetCandle APIGetCandle
type APIGetCandle struct {
	logger     *base.Logger
	rpc        *rmq.RPCHandler
	authClient *user.AuthClient
}

const (
	apiPrefixCandle = "/api/v1/candle"
)

func (api *APIGetCandle) handleRequest(ctx *fasthttp.RequestCtx) {
	fullname := func(s string) string { return apiPrefixCandle + s }
	switch string(ctx.Path()) {
	case fullname("/get"):
		var params APIGetCandleParams
		err := json.Unmarshal(ctx.PostBody(), &params)
		if err != nil {
			api.logger.Trace("Unmarshal json error:", err)
		}
		var reply *message.Message
		if err == nil {
			api.logger.Trace("Try auth:", params, params.User.GetUser())
			err = api.authClient.AuthHTTP(ctx, params.User)
			if err != nil {
				return
			}
		}
		points, err := ctx.PostArgs().GetUint("points")
		if err != nil {
			err = nil
			points = 60
		}
		if err == nil {
			src := base.SourceByPeriod{
				Source:      params.Source,
				PointsCount: int64(points),
			}
			api.logger.Trace("Request prices for:", params)
			reply, err = api.rpc.Handle(message.KindPriceCandleGetBySource, &src, true)
		}
		var body json.RawMessage
		if err == nil {
			if reply.Kind != message.KindPriceError {
				body = reply.Obj
			} else {
				err = errors.New("Get price error")
			}
		}
		base.HandleBody(ctx, err, body)
	default:
		ctx.Error("not found", fasthttp.StatusNotFound)
	}
}

//Stop Stop
func (api *APIGetCandle) Stop() {
	api.rpc.Stop()
}

//Tool Tool
func (api *APIGetCandle) Tool(args []string, flags *flag.FlagSet, isStopped chan int) (progressFunc func() error, err error) {
	base.LoggerParamsAdd(flags)

	bindAddr := flags.String("bind", "localhost:8005", "Address to bind")
	rmq.AddFlagsRPC(flags, "price")
	rmq.AddFlagsRPC(flags, "auth")
	flags.Usage = flags.PrintDefaults
	if err = flags.Parse(args); err != nil {
		return
	}
	logger := base.NewLoggerFromFlags(flags)
	api.logger = logger

	api.rpc, err = rmq.NewRPCHandlerFromFlags(logger, flags, "price")
	if err != nil {
		return nil, err
	}
	authRPC, err := rmq.NewRPCHandlerFromFlags(logger, flags, "auth")
	if err != nil {
		return nil, err
	}
	api.authClient = &user.AuthClient{AuthRPC: authRPC}
	go api.rpc.AsyncConsume(&isStopped)
	go api.authClient.Await(&isStopped)
	go func() {
		logger.Info("Start listening", *bindAddr)
		if err := fasthttp.ListenAndServe(*bindAddr, api.handleRequest); err != nil {
			log.Fatal(err)
		}
		isStopped <- 1
	}()
	progressFunc = func() error {
		return nil
	}

	logger.Info("AlertSaver tool init ok")
	return
}
