package price

import (
	"cryptoalerts/base"
	"cryptoalerts/message"
	"cryptoalerts/rmq"
	"encoding/json"

	"github.com/pkg/errors"

	"gopkg.in/mgo.v2"

	"gopkg.in/mgo.v2/bson"

	"github.com/namsral/flag"
)

//DBSaver DBSaver
type DBSaver struct {
	consumer    *rmq.Consumer
	sessionPool *base.MgoSessionPool
	db          base.DB
	logger      *base.Logger
}

func selectorByIDAndUser(id string, user base.UserInfo) bson.M {
	return bson.M{"_id": id, "user": bson.M{"id": user.ID}}
}

func savePrice(sessionPool *base.MgoSessionPool, price *base.Price) error {
	err := sessionPool.Run(func(sess *mgo.Session) error {
		dbName := price.Source.Market
		cur := price.Source.Currency
		if cur.Valid() {
			collection := string(cur.Base[0]) + string(cur.Quoted[0])
			data := base.PriceDB{
				Currency: price.Source.Currency.ToString(),
				TimeInMs: price.Quote.TimeInMs,
				Price:    price.Quote.Price,
			}
			err := sess.DB(dbName).C(collection).Insert(data)
			return errors.Wrap(err, "Insert")
		}
		return errors.New("incorrect price")
	})
	if err == nil {
		err = sessionPool.RunNotClone(func(sess *mgo.Session) error {
			dbName := price.Source.Market
			cur := price.Source.Currency
			forInsert := struct {
				Base   string `json:"base" bson:"base"`
				Quoted string `json:"quoted" bson:"quoted"`
				ID     string `bson:"_id"`
			}{
				Base:   cur.Base,
				Quoted: cur.Quoted,
				ID:     cur.ToString(),
			}
			_ = sess.DB(dbName).C(AllSymbolsTable).Insert(&forInsert)
			return nil
		})
	}
	return err
}

//OnMessage OnMessage
func (s *DBSaver) OnMessage(mess message.Message) (err error) {
	logger := s.logger
	logger.Trace("mess.Kind", mess.Kind)
	switch mess.Kind {
	case message.KindPriceAdd:
		var price base.Price
		err = json.Unmarshal(mess.Obj, &price)
		if err != nil {
			logger.Error("Unmarshal price error:", err, "obj:", mess.Obj)
		}
		savePrice(s.sessionPool, &price)
	default:
		s.logger.Warning("Unknown message:", mess.Kind, mess.Obj)
	}
	return
}

//Stop Stop
func (s *DBSaver) Stop() {
	s.consumer.StopAndClose()
	s.sessionPool.Close()

}

//Output Output
func (s *DBSaver) Output(calldepth int, st string) error {
	s.logger.Debug("mgo:", calldepth, st)
	return nil
}

//Tool Tool
func (s *DBSaver) Tool(args []string, flags *flag.FlagSet, isStopped chan int) (progressFunc func() error, err error) {

	base.LoggerParamsAdd(flags)
	base.AddDBFlags(flags)
	rmq.AddFlags(flags, "con", rmq.IsConsumer, "receive prices")
	workers := flags.Int("workers", 8, "workesrs for handle requests")

	flags.Usage = flags.PrintDefaults
	if err = flags.Parse(args); err != nil {
		return
	}
	logger := base.NewLoggerFromFlags(flags)
	s.logger = logger
	// mgo.SetDebug(true)
	// mgo.SetLogger(s)
	s.consumer, err = rmq.NewConsumerFromFlags(logger, flags, "con")
	if err != nil {
		return
	}

	s.db = base.MakeDBFromFlags(flags)
	logger.Info("Try create DB saver", s.db)
	s.sessionPool, err = base.NewMgoSessionPool(s.db)
	if err != nil {
		return
	}
	logger.Info("DB saver created")
	for i := 0; i < *workers; i++ {
		go func() {
			logger.Info("Start consume")
			err := s.consumer.Consume(s)
			if err != nil {
				s.consumer.StopAndClose()
				return
			}
		}()
	}
	progressFunc = func() error {
		if s.consumer.IsStopped() {
			return errors.New("Consumer is stopped")
		}
		return nil
	}

	logger.Info("AlertSaver tool init ok")
	return
}
