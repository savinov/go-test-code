package price

import (
	"cryptoalerts/base"
	"cryptoalerts/message"
	"cryptoalerts/rmq"
	"encoding/json"
	"math"
	"sync"

	"github.com/pkg/errors"

	"gopkg.in/mgo.v2"

	"github.com/namsral/flag"
)

type candle struct {
	min           float32
	max           float32
	open          float32
	timeOpen      int64
	close         float32
	timeClose     int64
	volume        int64
	startTimeInMS int64
}
type periodCandle struct {
	candles         []*candle
	length          int64
	chartPeriodInMS int64
	name            string
}

//CandleCache CandleCache
type CandleCache struct {
	consumer        *rmq.Consumer
	publisher       *rmq.Publisher
	inPriceConsumer *rmq.Consumer
	session         *base.MgoSessionPool
	logger          *base.Logger
	mutex           sync.Mutex

	BySymbolCharts map[string][]*periodCandle
}

func addPriceCandle(logger *base.Logger, periods []*periodCandle, quote price) {
	curTime := quote.GetTimeInMS()
	for _, p := range periods {
		if len(p.candles) == 0 {
			continue
		}
		// logger.Trace(p)
		last := p.candles[len(p.candles)-1]
		nextTime := last.startTimeInMS + p.chartPeriodInMS
		endOfCurrentPeriod := nextTime < curTime
		if endOfCurrentPeriod {
			toDelete := p.candles[0]
			copy(p.candles[0:], p.candles[1:])
			previous := p.candles[len(p.candles)-2]
			pr := quote.GetPrice()
			toDelete.min = pr
			toDelete.max = pr
			toDelete.open = previous.close
			toDelete.close = pr
			toDelete.timeOpen = quote.GetTimeInMS()
			toDelete.timeClose = quote.GetTimeInMS()
			toDelete.startTimeInMS = nextTime
			p.candles[len(p.candles)-1] = toDelete
		} else {
			added := false
			for i, candle := range p.candles {
				addedToPrevious := added
				added = false
				if candle.startTimeInMS <= curTime && curTime <= candle.startTimeInMS+p.chartPeriodInMS {

					if quote.GetTimeInMS() < candle.timeOpen {
						candle.timeOpen = quote.GetTimeInMS()
						candle.open = quote.GetPrice()
					}
					if addedToPrevious {
						candle.open = p.candles[i-1].close
						candle.max = base.MaxFloat32(candle.max, candle.open)
						candle.min = base.MinFloat32(candle.min, candle.open)
					}
					if quote.GetTimeInMS() > candle.timeClose {
						candle.timeClose = quote.GetTimeInMS()
						candle.close = quote.GetPrice()
					}

					candle.max = base.MaxFloat32(candle.max, quote.GetPrice())
					candle.min = base.MinFloat32(candle.min, quote.GetPrice())
					candle.volume++
					added = true
				}
			}
		}
	}
}

func (s *CandleCache) makeCachePeriods(endInMS, chartPoints int64) []*periodCandle {
	periods := make([]*periodCandle, 6, 6)
	periods[0] = &periodCandle{name: "hour", length: 60 * 60, chartPeriodInMS: 60}
	periods[1] = &periodCandle{name: "day", length: 24 * 60 * 60, chartPeriodInMS: 15 * 60}
	periods[2] = &periodCandle{name: "week", length: 7 * 24 * 60 * 60, chartPeriodInMS: 2 * 60 * 60}
	periods[3] = &periodCandle{name: "month", length: 31 * 24 * 60 * 60, chartPeriodInMS: 12 * 60 * 60}
	periods[4] = &periodCandle{name: "year", length: 365 * 24 * 60 * 60, chartPeriodInMS: 3 * 24 * 60 * 60}
	periods[5] = &periodCandle{name: "all", length: 2 * 365 * 24 * 60 * 60, chartPeriodInMS: 6 * 24 * 60 * 60}

	for _, p := range periods {
		var c int64
		p.chartPeriodInMS *= 1000
		p.length *= 5 * 1000

		candlesCount := p.length / p.chartPeriodInMS
		for c = candlesCount; c > 0; c-- {
			cand := &candle{startTimeInMS: endInMS - p.length, min: math.MaxFloat32}
			cand.min = math.MaxFloat32
			cand.timeOpen = math.MaxInt64
			p.candles = append(p.candles, cand)
		}
		periods = append(periods, p)
	}
	return periods
}

func (s *CandleCache) loadCacheForExchange(exchange string) error {
	logger := s.logger
	s.BySymbolCharts = make(map[string][]*periodCandle)
	curs := make([]base.Currency, 0)
	err := s.session.Run(func(sess *mgo.Session) error {
		return sess.DB(exchange).C(AllSymbolsTable).Find(nil).All(&curs)
	})
	if err != nil {
		return errors.Wrap(err, "load symbols")
	}
	timeInMS := base.MakeTimestampInMS()
	logger.Info("Currencies:", len(curs))
	for i, cur := range curs {
		quotes, err := loadPoints(s.session,
			base.PriceSource{Market: exchange, Currency: cur},
			base.Period{StartInMS: 0, EndInMS: timeInMS},
		)
		if err != nil {
			s.logger.Error("Error load price:", err)
		}
		if len(quotes) == 0 {
			continue
		}
		logger.Info(exchange+cur.ToString(), " quotes:", len(quotes), "i:", i, "len:", len(curs))

		endTime := quotes[len(quotes)-1].TimeInMs
		s.mutex.Lock()
		periods := s.makeCachePeriods(endTime, PointsCount)
		for _, q := range quotes {
			addPriceCandle(logger, periods, q)
		}
		name := exchange + cur.ToString()
		s.BySymbolCharts[name] = periods
		s.mutex.Unlock()
	}
	return nil
}

func makeCandles(p *periodCandle) [][]interface{} {
	one := make([][]interface{}, 0, len(p.candles))
	for _, v := range p.candles {
		if v.volume > 0 {
			one = append(one, []interface{}{v.open, v.max, v.min, v.close, v.volume, v.startTimeInMS, p.chartPeriodInMS})
		}
	}
	return one
}

//OnMessage OnMessage
func (s *CandleCache) OnMessage(mess message.Message) (err error) {
	logger := s.logger
	logger.Trace("mess.Kind", mess.Kind.ToString())

	switch mess.Kind {
	case message.KindPriceCandleGetBySource:
		var srcByPeriod base.SourceByPeriod
		err = json.Unmarshal(mess.Obj, &srcByPeriod)
		if err != nil {
			logger.Trace("Unmarshal json error:", err)
		}
		src := srcByPeriod.Source
		var messReply message.Message
		if src.GetStrID() == "" {
			err = errors.New("incorrect incoming name")
		}
		name := string(srcByPeriod.Source.GetStrID())
		logger.Debug("get cache for:", name)
		if err == nil {
			out := make(map[string][][]interface{})
			s.mutex.Lock()
			periods := s.BySymbolCharts[name]
			for _, p := range periods {
				out[p.name] = makeCandles(p)
			}
			s.mutex.Unlock()
			messReply, err = message.Make(message.KindPriceGetBySource, out, mess.ID, mess.ReplyTo)
		}
		if err != nil {
			messReply, err = message.Make(message.KindPriceError, err.Error(), mess.ID, mess.ReplyTo)
		}
		if err != nil {
			logger.Debug("Error getPrice:", err)
		}
		s.publisher.Publish(mess.ReplyTo, messReply)
	case message.KindPriceAdd:
		var price base.Price
		err = json.Unmarshal(mess.Obj, &price)
		if err != nil {
			logger.Error("Unmarshal price error:", err, "obj:", mess.Obj)
		}
		s.mutex.Lock()
		periods := s.BySymbolCharts[string(price.Source.GetStrID())]
		addPriceCandle(logger, periods, &price)
		s.mutex.Unlock()
	default:
		s.logger.Warning("Unknown message:", mess.Kind, mess.Obj)
	}
	return
}

//Stop Stop
func (s *CandleCache) Stop() {
	s.consumer.StopAndClose()
	s.publisher.Close()
	s.session.Close()

}

//Tool Tool
func (s *CandleCache) Tool(args []string, flags *flag.FlagSet, isStopped chan int) (progressFunc func() error, err error) {
	base.LoggerParamsAdd(flags)
	base.AddDBFlags(flags)
	rmq.AddFlags(flags, "con", rmq.IsConsumer, "receive price load commands")
	rmq.AddFlags(flags, "pub", rmq.IsPubliher, "send reply")

	rmq.AddFlags(flags, "price", rmq.IsConsumer, "receive prices")

	workers := flags.Int("workers", 8, "workesrs for handle requests")
	var exchanges base.FlagArray
	flags.Var(&exchanges, "exchange", "can be multiple")
	flags.Usage = flags.PrintDefaults
	if err = flags.Parse(args); err != nil {
		return
	}
	logger := base.NewLoggerFromFlags(flags)
	s.logger = logger
	s.consumer, err = rmq.NewConsumerFromFlags(logger, flags, "con")
	if err != nil {
		return
	}
	s.inPriceConsumer, err = rmq.NewConsumerFromFlags(logger, flags, "price")
	if err != nil {
		return
	}
	db := base.MakeDBFromFlags(flags)
	logger.Info("Try create DB saver", db)
	s.session, err = base.NewMgoSessionPool(db)
	if err != nil {
		return
	}

	startConsume := func(consumer *rmq.Consumer) {
		logger.Info("Start consume")
		err := consumer.Consume(s)
		if err != nil {
			isStopped <- 1
			return
		}
		isStopped <- 1
	}
	go startConsume(s.inPriceConsumer)

	logger.Info("DB saver created")
	logger.Info("Start make cahce")
	for _, exchange := range exchanges {
		logger.Info("Start make cahce for:", exchange)
		go s.loadCacheForExchange(exchange)
	}
	logger.Info("Cahce created")
	s.publisher, err = rmq.NewPublisherFromFlags(logger, flags, "pub")
	if err != nil {
		return
	}

	for i := 0; i < *workers/2-1; i++ {
		go startConsume(s.consumer)
	}
	progressFunc = func() error {

		return nil
	}

	logger.Info("AlertSaver tool init ok")
	return
}
