package price

import (
	"cryptoalerts/base"
	"cryptoalerts/message"
	"cryptoalerts/rmq"
	"encoding/json"
	"math"
	"sync"

	"github.com/bradfitz/slice"

	"github.com/pkg/errors"

	"gopkg.in/mgo.v2"

	"gopkg.in/mgo.v2/bson"

	"github.com/namsral/flag"
)

type avg struct {
	min           float32
	max           float32
	minTimeInMS   int64
	maxTimeInMS   int64
	startTimeInMS int64
}
type period struct {
	data            []*avg
	candles         []*candle
	length          int64
	chartPeriodInMS int64
	name            string
}

//DBGetter DBGetter
type DBGetter struct {
	consumer        *rmq.Consumer
	publisher       *rmq.Publisher
	inPriceConsumer *rmq.Consumer
	session         *base.MgoSessionPool
	logger          *base.Logger
	mutex           sync.Mutex

	BySymbolCharts map[string][]*period
}

type price interface {
	GetPrice() float32
	GetTimeInMS() int64
}

func addPrice(logger *base.Logger, periods []*period, quote price) {
	curTime := quote.GetTimeInMS()
	for _, p := range periods {
		if p.name == "hour" {
		}
		// logger.Trace(p)
		last := p.data[len(p.data)-1]
		nextTime := last.startTimeInMS + p.chartPeriodInMS
		endOfCurrentPeriod := nextTime < curTime
		if endOfCurrentPeriod {
			avgToDelete := p.data[0]
			copy(p.data[0:], p.data[1:])
			avgToDelete.min = quote.GetPrice()
			avgToDelete.max = quote.GetPrice()
			avgToDelete.minTimeInMS = quote.GetTimeInMS()
			avgToDelete.maxTimeInMS = quote.GetTimeInMS()
			avgToDelete.startTimeInMS = nextTime
			p.data[len(p.data)-1] = avgToDelete
		} else {
			for _, avg := range p.data {
				if avg.startTimeInMS <= curTime && curTime < avg.startTimeInMS+p.chartPeriodInMS {
					if quote.GetPrice() > avg.max {
						avg.max = quote.GetPrice()
						avg.maxTimeInMS = quote.GetTimeInMS()
					} else if quote.GetPrice() < avg.min {
						avg.min = quote.GetPrice()
						avg.minTimeInMS = quote.GetTimeInMS()
					}
				}
			}
		}
	}
}

func makeCachePeriods(endInMS, chartPoints int64) []*period {
	periods := make([]*period, 6, 6)
	periods[0] = &period{name: "hour", length: 60 * 60, chartPeriodInMS: 120}
	periods[1] = &period{name: "day", length: 24 * 60 * 60, chartPeriodInMS: 24 * 120}
	periods[2] = &period{name: "week", length: 7 * 24 * 60 * 60, chartPeriodInMS: 7 * 24 * 120}
	periods[3] = &period{name: "month", length: 31 * 24 * 60 * 60, chartPeriodInMS: 31 * 24 * 120}
	periods[4] = &period{name: "year", length: 365 * 24 * 60 * 60, chartPeriodInMS: 365 * 24 * 120}
	periods[5] = &period{name: "all", length: 2 * 365 * 24 * 60 * 60, chartPeriodInMS: 2 * 365 * 24 * 120}

	for _, p := range periods {
		var c int64
		p.chartPeriodInMS *= 1000
		for c = chartPoints; c > 0; c-- {
			p.data = append(p.data, &avg{startTimeInMS: endInMS - c*p.chartPeriodInMS, min: math.MaxFloat32})
		}
		periods = append(periods, p)
	}
	return periods
}

func loadPoints(mongo *base.MgoSessionPool, source base.PriceSource, period base.Period) ([]*base.PriceDB, error) {
	// logger := s.logger
	var quotes []*base.PriceDB
	err := mongo.Run(func(sess *mgo.Session) error {
		db := source.Market
		//base.PriceDB
		cur := source.Currency
		if !cur.Valid() {
			return errors.New("Incorrect quoted symbol")
		}

		col := string(cur.Base[0]) + string(cur.Quoted[0])
		symbol := bson.M{"c": cur.ToString()}
		query := sess.DB(db).C(col).Find(symbol)
		return query.All(&quotes)
	})
	slice.Sort(quotes, func(i, j int) bool { return quotes[i].TimeInMs < quotes[j].TimeInMs })
	return quotes, err
}

const (
	AllSymbolsTable   = "all"
	AllExchangesDB    = "configuration"
	AllExchangesTable = "exchanges"
	PointsCount       = 150
)

func (s *DBGetter) loadCacheForExchange(exchange string) error {
	logger := s.logger
	s.BySymbolCharts = make(map[string][]*period)
	curs := make([]base.Currency, 0)
	err := s.session.Run(func(sess *mgo.Session) error {
		return sess.DB(exchange).C(AllSymbolsTable).Find(nil).All(&curs)
	})
	if err != nil {
		return errors.Wrap(err, "load symbols")
	}
	timeInMS := base.MakeTimestampInMS()
	logger.Info("Currencies:", len(curs))
	for i, cur := range curs {
		quotes, err := loadPoints(s.session,
			base.PriceSource{Market: exchange, Currency: cur},
			base.Period{StartInMS: 0, EndInMS: timeInMS},
		)
		if err != nil {
			s.logger.Error("Error load price:", err)
		}
		if len(quotes) == 0 {
			continue
		}
		logger.Info(exchange+cur.ToString(), " quotes:", len(quotes), "i:", i, "len:", len(curs))

		endTime := quotes[len(quotes)-1].TimeInMs
		s.mutex.Lock()
		periods := makeCachePeriods(endTime, PointsCount)
		for _, q := range quotes {
			addPrice(logger, periods, q)
		}
		name := exchange + cur.ToString()
		s.BySymbolCharts[name] = periods
		s.mutex.Unlock()
	}
	return nil
}
func makeChart(p *period) [][]interface{} {
	one := make([][]interface{}, 0, len(p.data))
	for _, v := range p.data {
		if !base.FloatEquals32(v.max, 0.0) {
			if v.minTimeInMS > v.maxTimeInMS {
				one = append(one, []interface{}{v.max, v.maxTimeInMS})
				one = append(one, []interface{}{v.min, v.minTimeInMS})
			} else {
				one = append(one, []interface{}{v.min, v.minTimeInMS})
				one = append(one, []interface{}{v.max, v.maxTimeInMS})
			}
		}
	}
	return one
}
func makeCandle(p *period) [][]interface{} {
	one := make([][]interface{}, 0, len(p.candles))
	for _, v := range p.candles {
		if v.volume > 0 {
			one = append(one, []interface{}{v.open, v.max, v.min, v.close, v.volume, v.startTimeInMS})
		}
	}
	return one
}

//OnMessage OnMessage
func (s *DBGetter) OnMessage(mess message.Message) (err error) {
	logger := s.logger
	logger.Trace("mess.Kind", mess.Kind.ToString())

	switch mess.Kind {
	case message.KindPriceGetBySource:
		var srcByPeriod base.SourceByPeriod
		err = json.Unmarshal(mess.Obj, &srcByPeriod)
		if err != nil {
			logger.Trace("Unmarshal json error:", err)
		}
		src := srcByPeriod.Source
		var messReply message.Message
		if src.GetStrID() == "" {
			err = errors.New("incorrect incoming name")
		}
		name := string(srcByPeriod.Source.GetStrID())
		logger.Debug("get cache for:", name)
		if err == nil {
			out := make(map[string][][]interface{})
			s.mutex.Lock()
			periods := s.BySymbolCharts[name]
			for _, p := range periods {
				out[p.name] = makeChart(p)
			}
			s.mutex.Unlock()
			messReply, err = message.Make(message.KindPriceGetBySource, out, mess.ID, mess.ReplyTo)
		}
		if err != nil {
			messReply, err = message.Make(message.KindPriceError, err.Error(), mess.ID, mess.ReplyTo)
		}
		if err != nil {
			logger.Debug("Error getPrice:", err)
		}
		s.publisher.Publish(mess.ReplyTo, messReply)
	case message.KindPriceAdd:
		var price base.Price
		err = json.Unmarshal(mess.Obj, &price)
		if err != nil {
			logger.Error("Unmarshal price error:", err, "obj:", mess.Obj)
		}
		s.mutex.Lock()
		periods := s.BySymbolCharts[string(price.Source.GetStrID())]
		addPrice(logger, periods, &price)
		s.mutex.Unlock()
	default:
		s.logger.Warning("Unknown message:", mess.Kind, mess.Obj)
	}
	return
}

//Stop Stop
func (s *DBGetter) Stop() {
	s.consumer.StopAndClose()
	s.publisher.Close()
	s.session.Close()

}

//Tool Tool
func (s *DBGetter) Tool(args []string, flags *flag.FlagSet, isStopped chan int) (progressFunc func() error, err error) {
	base.LoggerParamsAdd(flags)
	base.AddDBFlags(flags)
	rmq.AddFlags(flags, "con", rmq.IsConsumer, "receive price load commands")
	rmq.AddFlags(flags, "pub", rmq.IsPubliher, "send reply")

	rmq.AddFlags(flags, "price", rmq.IsConsumer, "receive prices")

	workers := flags.Int("workers", 8, "workesrs for handle requests")
	var exchanges base.FlagArray
	flags.Var(&exchanges, "exchange", "can be multiple")
	flags.Usage = flags.PrintDefaults
	if err = flags.Parse(args); err != nil {
		return
	}
	logger := base.NewLoggerFromFlags(flags)
	s.logger = logger
	s.consumer, err = rmq.NewConsumerFromFlags(logger, flags, "con")
	if err != nil {
		return
	}
	s.inPriceConsumer, err = rmq.NewConsumerFromFlags(logger, flags, "price")
	if err != nil {
		return
	}
	db := base.MakeDBFromFlags(flags)
	logger.Info("Try create DB saver", db)
	s.session, err = base.NewMgoSessionPool(db)
	if err != nil {
		return
	}

	startConsume := func(consumer *rmq.Consumer) {
		logger.Info("Start consume")
		err := consumer.Consume(s)
		if err != nil {
			isStopped <- 1
			return
		}
		isStopped <- 1
	}
	go startConsume(s.inPriceConsumer)

	logger.Info("DB saver created")
	logger.Info("Start make cahce")
	for _, exchange := range exchanges {
		logger.Info("Start make cahce for:", exchange)
		go s.loadCacheForExchange(exchange)
	}
	logger.Info("Cahce created")
	s.publisher, err = rmq.NewPublisherFromFlags(logger, flags, "pub")
	if err != nil {
		return
	}

	for i := 0; i < *workers/2-1; i++ {
		go startConsume(s.consumer)
	}
	progressFunc = func() error {

		return nil
	}

	logger.Info("AlertSaver tool init ok")
	return
}
