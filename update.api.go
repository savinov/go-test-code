package price

import (
	"cryptoalerts/base"
	"cryptoalerts/message"
	"cryptoalerts/rmq"
	"encoding/json"
	"log"

	"github.com/pkg/errors"

	"github.com/namsral/flag"
	"github.com/valyala/fasthttp"
)

//APIUpdate APIUpdate
type APIUpdate struct {
	logger    *base.Logger
	publisher *rmq.Publisher
}

func (api *APIUpdate) handleRequest(ctx *fasthttp.RequestCtx) {
	fullname := func(s string) string { return "/api/v1/addprice" + s }
	path := string(ctx.Path())
	switch path {
	case fullname("/add"):
		var price base.Price
		data, _ := json.Marshal(price)
		api.logger.Trace(string(data))
		body := ctx.PostBody()
		api.logger.Trace("Data len:", len(body), " path:", path, string(ctx.Method()))
		err := json.Unmarshal(body, &price)
		if err != nil {
			err = errors.Wrap(err, "unmarshal price error"+string(body))
		}
		var mess message.Message
		if err == nil && !price.Valid() {
			err = errors.New("Error price")
		}
		if err == nil {
			mess, err = message.Make(message.KindPriceAdd, &price, "", "")
			if err != nil {
				err = errors.Wrap(err, "make message error")
			}
		}
		if err == nil {
			api.logger.Debug("Add price:", price)
			err = api.publisher.Publish("", mess)
			if err != nil {
				err = errors.Wrap(err, "publish error")
			}
		}
		base.HandleBody(ctx, err, nil)
	default:
		ctx.Error("not found", fasthttp.StatusNotFound)
	}
}

//Stop Stop
func (api *APIUpdate) Stop() {
	api.publisher.Close()
}

//Tool Tool
func (api *APIUpdate) Tool(args []string, flags *flag.FlagSet, isStopped chan int) (progressFunc func() error, err error) {

	base.LoggerParamsAdd(flags)

	bindAddr := flags.String("bind", "localhost:8002", "Address to bind")

	rmq.AddFlags(flags, "pub", rmq.IsPubliher, "send price commands")
	flags.Usage = flags.PrintDefaults
	if err = flags.Parse(args); err != nil {
		return
	}
	logger := base.NewLoggerFromFlags(flags)
	api.logger = logger
	api.publisher, err = rmq.NewPublisherFromFlags(logger, flags, "pub")
	if err != nil {
		return
	}
	go func() {
		logger.Info("Start listening", *bindAddr)
		if err := fasthttp.ListenAndServe(*bindAddr, api.handleRequest); err != nil {
			log.Fatal(err)
		}
		isStopped <- 1
	}()
	progressFunc = func() error {
		return nil
	}

	logger.Info("AlertSaver tool init ok")
	return
}
